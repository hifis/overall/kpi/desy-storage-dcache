# DESY Storage - dCache

User and Usage numbers for DESY Storage, based on dCache.

See also: 

- Documentation: <https://hifis.net/doc/cloud-services/Storage_DESY/>
- Contact: [support@hifis.net](mailto:support@hifis.net?subject=[hifis-storage])

Data reported:

* Stored data / Usage in group folders that belong to groups in Helmholtz AAI:
    - 2nd column: Overall usage [TiB]
    - 3rd column: from this: Usage [TiB] by groups that are [registered in Helmholtz AAI in non-Helmholtz namespace](https://hifis.net/doc/helmholtz-aai/list-of-vos/#other-supported-vos) (e.g. NFDI groups)
* Total Usage [TiB]: This comprises all data stored on this instance (incl. non Helmholtz usage from other federations and DESY internal usage)
* Users: Number of distinct Helmholtz AAI groups (including sub-groups) with data in storage.
